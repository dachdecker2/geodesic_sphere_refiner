
# global imports
import math as m
import collections as c
from functools import reduce
# local imports
import point

class too_few_corners(BaseException):
    '''a face must have at least 3 corners for obtaining triangles'''

class mesh(object):
    def __init__(self, inputmesh, refine_with_arc_instead_of_chord=False, size=m.pi):
        faces = inputmesh[1]
        self.points = inputmesh[0]
        self.edges = []
        self.triangles = []
        self.use_arc_not_chord = refine_with_arc_instead_of_chord
        self.size = size
        def add_edge(a, b):
            if [a, b] in self.edges: return self.edges.index([a, b])
            if [b, a] in self.edges: return self.edges.index([b, a])
            self.edges.append([a, b]); return len(self.edges)-1
        for f in range(len(faces)):
            if len(faces[f]) < 3:
                raise too_few_corners
            elif len(faces[f]) == 3: # this face is already triangle and can be used as is
                f_edges = []
                for c in range(len(faces[f])):
                    f_edges.append(add_edge(faces[f][c-1], faces[f][c]))
                self.triangles.append(tuple(f_edges))
                if self.size == 'triangle':
                    self.size = m.pi # after the input, m.size should be float for later use
                    return
            else: # this face has more than 3 corners, it is replaced by triangle to a new point in the center of the face
                corners = map(lambda c: c.cartesian(), select_objects(faces[f], self.points))
                center = map(lambda d: d/len(corners), reduce(lambda a, b: [a[0]+b[0], a[1]+b[1], a[2]+b[2]], corners))
                center = point.cartesian_point(*center)
                center.r = sum(map(lambda d: d**2, corners[0]))**0.5
                center_point_no = len(self.points)
                self.points.append(center)
                for c in range(len(corners)):
                    f_edges = []
                    f_edges.append(add_edge(faces[f][c-1], faces[f][c]))
                    f_edges.append(add_edge(center_point_no, faces[f][c]))
                    self.triangles.append(f_edges + [add_edge(center_point_no, faces[f][c-1])])
                    if self.size == 'triangle':
                        self.size = m.pi # after the input, m.size should be float for later use
                        return

    def set_radius(self, r):
        for p in self.points:
            p.r = r

    def rotate_around_origin(self, axis, angel):
        self.points = map(lambda p: p.rotate_around_origin(axis, angel), self.points)

    def get_edge_length(self, e):
        '''returns the length of the edge identified by its number'''
        return self.points[e[0]].distance(self.points[e[1]])

    def count_lengths(self):
        # return counter object with distribution of edge lengths
        lengths = map(self.get_edge_length, self.edges) # get edge lengths
        lengths = map(lambda l: round(l, 4), lengths) # round to n decimal places
        return c.Counter(lengths)

    def __str__(self, points=None, edges=None, triangles=None):
        # list points, edges and triangles, if not given, use internals
        p = ['points'] + list_objects(points or self.points)
        e = ['edges'] + list_objects(edges or self.edges)
        t = ['triangles'] + list_objects(triangles or self.triangles)
        l = max(map(len, [p, e, t]))
        p, e, t = map(lambda li: li+['']*(l-len(li)), [p, e, t])
        return ''.join(map(lambda p, e, t: '%-30s%-20s%-20s\n' % (p, e, t), p, e, t))

    def display_triangle(self, baselines_points, edges=None):
        linecount = len(baselines_points)
        point_lines = []
        edge_lines = []
        triangle_lines = []
        for l in range(linecount):
            point_lines.append('  ' * (linecount - l) + ''.join(map(lambda n: ' %3d' % n, baselines_points[l])))

        point_lines_len = max(map(len, point_lines)+[0])
        edge_lines_len = max(map(len, edge_lines)+[0])
        triangle_lines_len = max(map(len, triangle_lines)+[0])
        for l in range(linecount):
            print('%%-%ds' % point_lines_len) % point_lines[l]
            print()

    def output_beams(self, r=0.02, size=m.pi):
        # line format: 'beam([x, y, z],[x,y,z], r);'
        def beam(edge):
            points = reduce(lambda a, b: a + b, map(lambda e: list(e.cartesian()), select_objects(edge, self.points)))
            return 'beam([%f, %f, %f], [%f, %f, %f], %f);' % tuple(points + [r])
        relevant_edges = filter(lambda e: self.points[e[0]].theta<=size and self.points[e[1]].theta<=size, self.edges)
        return '\n'.join(map(beam, relevant_edges))

    def output_scad_polyhedron(self, size=''):
        point_zs = map(lambda p: (p[0], p[1].theta), enumerate(self.points))
        point_zs = filter(lambda p: p[1] <= size, point_zs)
        point_zs = map(lambda p: p[0], point_zs) # reduce to the number of the relevant points
        points = select_objects(point_zs, self.points)
        points = ',\n'.join(map(lambda p: '      [%0.4f, %0.4f, %0.4f]' % (p.cartesian()), points))
        triangles = map(lambda t: list(select_objects(t, self.edges)), self.triangles)
        triangles = map(lambda t: list(set(reduce(lambda a, b: a+b, t))), triangles)
        triangles = filter(lambda t: all(map(lambda a: a in point_zs, t)), triangles)
        triangles = map(lambda t: map(lambda p: point_zs.index(p), t), triangles)
        triangles = '      ' + ',\n      '.join(map(str, triangles))
        return 'polyhedron(\n   points = [\n%s\n   ],\n   triangles = [\n%s\n   ]\n);' % (points, triangles)

    def output_prginput(self, size):
        return '[[\n%s],\n[%s]\n]' % (',\n'.join(map(lambda p: '   %.4f, m.pi*%.4f, m.pi*%.4f' % (p.r, p.theta/m.pi, p.phi/m.pi), self.points)), self.triangles)

    def output_prginput_cartesian(self, size):
        return '[[%s], [%s]]' % (self.points, self.triangles)

    def output(self, fmt, size):
        # fmt in prginput|humanreadable|beams|scad
#        print('fmt: %s, size: %s' % (fmt, size))
        if fmt == 'scad':     return self.output_scad_polyhedron(size)
        if fmt == 'beams':    return self.output_beams(self.points[0].length()/24, size)
        if fmt == 'prginput': return self.output_prginput(size)
#        if fmt == 'humanreadable': return self.output_prginput(size)
        return 'fmt <%s> not implemented, yet' % fmt


    def refine(self, order=1):
        # obtain new points on the edges
        edges = []
        def add_edge(a, b):
            if [a, b] in edges: return edges.index([a, b])
            if [b, a] in edges: return edges.index([b, a])
            edges.append([a, b]); return len(edges)-1
        partitioned_edges = []
        for e in self.edges:
            new_points = partition_edge(self.points[e[0]], self.points[e[1]], order, self.use_arc_not_chord)[1:-1]
            partitioned_edges.append([e[0]] + list(range(len(self.points), len(self.points)+len(new_points))) + [e[1]])
            self.points.extend(new_points)
        # assign newly generated points to the triangles
        triangles = []
        for t in self.triangles: # loop over already existing  triangles
            e1, e2, e3 = select_objects(t, partitioned_edges) # obtain edges of actual triangles
            # arrange e1 and e2 to start at the common point and the third
            if e1[0] not in e2: e1.reverse() # swap edge to start at common point
            if e2[0] not in e1: e2.reverse() # swap edge to start at common point
            if e3[0] not in e1: e3.reverse() # swap edge to start at e1
            # obtain a complete pointmap for this new triangle
            baselines_points = [[e1[0]]] # top point of triangle
            for i in range(1, len(e1)-1):
                baseline_points = partition_edge(self.points[e1[i]], self.points[e2[i]], i, self.use_arc_not_chord)
                baseline_point_numbers = [e1[i]] + list(range(len(self.points), len(self.points)+len(baseline_points)-2)) + [e2[i]]
                self.points.extend(baseline_points[1:-1])
                baselines_points.append(baseline_point_numbers)
            baselines_points.append(e3)
#            self.display_triangle(baselines_points)

            # add edges and triangle covering the previously obtained pointmap
            for bl in range(1, len(baselines_points)): # loop over lines, starting at second line
                bl_len = len(baselines_points[bl])
                for bl_point in range(bl_len): # loop over points of aktuall starting at the second
                    if bl_point < bl_len - 1: # if not last point of a baseline, add edge to the top right
                        topright_edge = add_edge(baselines_points[bl][bl_point],
                                                 baselines_points[bl-1][bl_point])
                    if bl_point > 0: # if not the first point of a baseline, add edges to the left and top left
                        left_edge = add_edge(baselines_points[bl][bl_point],
                                             baselines_points[bl][bl_point-1])
                        topleft_edge = add_edge(baselines_points[bl][bl_point],
                                                baselines_points[bl-1][bl_point-1])
                        # if not the first point of a baseline, add triangle standing with its base on the baseline
                        triangles.append((left_edge,
                                          topleft_edge, 
                                          add_edge(baselines_points[bl][bl_point-1], 
                                                   baselines_points[bl-1][bl_point-1])))
                        if bl_point < bl_len - 1: # if not the first and not the last point of a baseline,
                                                  # add triangle standing with its tip on the actual point
                            triangles.append((add_edge(baselines_points[bl][bl_point],
                                                       baselines_points[bl-1][bl_point-1]),
                                              add_edge(baselines_points[bl-1][bl_point-1], 
                                                       baselines_points[bl-1][bl_point]),
                                              add_edge(baselines_points[bl-1][bl_point], 
                                                       baselines_points[bl][bl_point]),
                            ))

#        print('=== after ===\n' + self.__str__(edges=edges, triangles=triangles))
        self.edges = edges
        self.triangles = triangles


def select_objects(indizes, objects):
    '''returns elements out of a list of objects that are identified by thier indizes'''
    return map(lambda i: objects[i], indizes)

def list_objects(objects):
    '''returns a supplied list of objects one per line with the leading number'''
    return map(lambda e: '%3d: %s' % (e[0], str(e[1])), enumerate(objects))


def partition_edge_arc(start, end, order):
    '''returns an list of points including start and end partitioned into order times, equally distributed over the arc between start and end'''
    # obtain lengths of the edges and the angel in the center
    a = start.length()
    b = end.length()
    c = start.distance(end)
    alpha = m.acos((a**2 + b**2 - c**2)/(2*a*b)) # angel from center to be used for distribution
    normal_ab = start.cross_product(end)
    points=[]
    for i in range(order+1): # partition edge along the arc over itself
        points.append(start.rotate_around_origin(normal_ab, i*alpha/order))
    for p in points: p.r = start.r # set new points to be located right on the sphere
    return points

def partition_edge(start, end, order, use_arc_not_chord=False):
    '''returns an list of points including start and end partitioned into order times, equally distributed over the chord between start and end'''
    if use_arc_not_chord:
        return partition_edge_arc(start, end, order)
    x1, y1, z1 = start.cartesian()
    x2, y2, z2 = end.cartesian()
    xl, yl, zl = x2 - x1, y2 - y1, z2 - z1 # get length by direction
    points=[]
    for i in range(order+1): # partition edge along itself
        points.append(point.cartesian_point(x1+i*xl/order, y1+i*yl/order, z1+i*zl/order))
    for p in points: p.r = start.r # lift new points to sphere
    return points


phi = (1+5.**0.5)/2 # golden ratio
r = 1 # 2.**0.5
# tetraeder by 4 of the 8 corners of a cube
tetraeder_2 = [[
        point.point(r, 1*m.pi/8, 0*m.pi/2),
        point.point(r, 2*m.pi/8, 1*m.pi/2),
        point.point(r, 3*m.pi/8, 2*m.pi/2),
        point.point(r, 4*m.pi/8, 3*m.pi/2)
    ], [
        [0, 1, 2],
        [1, 2, 3],
        [0, 3, 2],
        [0, 1, 3]
    ]
]

# regular tetraeder (one tip to the top)
x = 0.60817344796939272
tetraeder = [[
        point.point(r, 0, 0),
        point.point(r, x*m.pi, 0*m.pi/3),
        point.point(r, x*m.pi, 2*m.pi/3),
        point.point(r, x*m.pi, 4*m.pi/3),
    ], [
        [0, 1, 2], [1, 2, 3], [0, 3, 2], [0, 1, 3],
    ]
]

cube = [[
        point.cartesian_point( 1, 1, 1),
        point.cartesian_point( 1,-1, 1),
        point.cartesian_point(-1,-1, 1),
        point.cartesian_point(-1, 1, 1),
        point.cartesian_point( 1, 1,-1),
        point.cartesian_point( 1,-1,-1),
        point.cartesian_point(-1,-1,-1),
        point.cartesian_point(-1, 1,-1),
    ], [
        [0, 1, 2, 3], [4, 5, 6, 7], [0, 4, 7, 3],
        [1, 5, 6, 2], [0, 1, 5, 4], [2, 3, 7, 6],
    ]
]

oktaeder = [[
        point.cartesian_point( 0, 0, 1),
        point.cartesian_point( 0, 0,-1),
        point.cartesian_point( 0, 1, 0),
        point.cartesian_point( 0,-1, 0),
        point.cartesian_point( 1, 0, 0),
        point.cartesian_point(-1, 0, 0),
    ], [
        [0, 2, 4], [0, 2, 5], [0, 3, 4], [0, 3, 5],
        [1, 2, 4], [1, 2, 5], [1, 3, 4], [1, 3, 5],
    ]
]

dodekaeder = [[
        point.cartesian_point( 1/phi,   phi,     0),
        point.cartesian_point(     1,     1,     1),
        point.cartesian_point(   phi,     0, 1/phi),
        point.cartesian_point(   phi,     0,-1/phi),
        point.cartesian_point(     1,     1,    -1),
        point.cartesian_point(     1,    -1,    -1),
        point.cartesian_point( 1/phi,  -phi,     0),
        point.cartesian_point(     1,    -1,     1),
        point.cartesian_point(     0, 1/phi,  -phi),
        point.cartesian_point(     0,-1/phi,  -phi),
        point.cartesian_point(     0, 1/phi,   phi),
        point.cartesian_point(     0,-1/phi,   phi),
        point.cartesian_point(    -1,     1,     1),
        point.cartesian_point(    -1,     1,    -1),
        point.cartesian_point(    -1,    -1,    -1),
        point.cartesian_point(    -1,    -1,     1),
        point.cartesian_point(  -phi,     0, 1/phi),
        point.cartesian_point(  -phi,     0,-1/phi),
        point.cartesian_point( -1/phi,   phi,    0),
        point.cartesian_point( -1/phi,  -phi,    0),
    ], [
        [ 0, 1, 2, 3, 4], [ 2, 3, 5, 6, 7], [ 3, 5, 9, 8, 4], [ 2, 1,10,11, 7],
        [ 9, 8,13,17,14], [10,11,15,16,12], [16,17,13,18,12], [16,17,14,19,15],
        [ 0, 4, 8,13,18], [ 0, 1,10,12,18], [ 6, 5, 9,14,19], [ 6, 7,11,15,19],
    ]
]

a = m.atan(2)
b = m.pi-a
ikosaeder = [[
        point.point(r,    0,        0),
        point.point(r,    a, 0*m.pi/5),
        point.point(r,    b, 1*m.pi/5),
        point.point(r,    a, 2*m.pi/5),
        point.point(r,    b, 3*m.pi/5),
        point.point(r,    a, 4*m.pi/5),
        point.point(r,    b, 5*m.pi/5),
        point.point(r,    a, 6*m.pi/5),
        point.point(r,    b, 7*m.pi/5),
        point.point(r,    a, 8*m.pi/5),
        point.point(r,    b, 9*m.pi/5),
        point.point(r, m.pi,        0),
    ], [
        [ 0, 1, 3], [ 0, 3, 5], [ 0, 5, 7], [ 0, 7, 9], [ 0, 9, 1],
        [ 1, 3, 2], [ 3, 5, 4], [ 5, 7, 6], [ 7, 9, 8], [ 9, 1,10],
        [ 1,10, 2], [ 3, 2, 4], [ 5, 4, 6], [ 7, 6, 8], [ 9, 8,10],
        [11, 2, 4], [11, 4, 6], [11, 6, 8], [11, 8,10], [11,10, 2],
    ]
]

initialbodys = 'tetraeder|oktaeder|cube|dodekaeder|ikosaeder'
outputformats = 'prginput|humanreadable|beams|scad'

if __name__ == '__main__':
#                               n -> tri/sphere    n -> tri/sphere   rotation
#    msh = mesh(tetraeder)   # 10 ->  400         16 ->    1024
#    msh = mesh(cube)        #  4 -> 384           6 ->     864
#    msh = mesh(oktaeder)
#    msh = mesh(dodekaeder)  #  3 -> 540           4 ->     960        m.pi/3.1
    msh = mesh(ikosaeder)   #  4 -> 320           6 ->     720
#    msh.refine(2)
#    msh.refine(2)
#    msh.refine(2)
#    print(msh)
    lengths = map(lambda e: msh.points[e[0]].distance(msh.points[e[1]]), msh.edges)
#    print('\n'.join(list_objects(map(lambda l: '%.24f' % l, lengths))))
#    print('%0.24f' % x, lengths[2] - lengths[1])
#    print('count of edge lengths:\n%s' % '\n'.join(map(lambda e: '%8.4f: %d' % (e[0], e[1]), sorted(m.count_lengths().items()))))
#    print msh.output_beams(0.05)

    msh.set_radius(4.5)
#    msh.rotate_around_origin(point.cartesian_point(1, 0, 0), m.pi/3.1)
    n = 8
#    msh.refine(2)
#    msh.refine(4)
    msh.refine(6)
#    msh.refine(6)
#    print(msh)
    lengths = sorted(msh.count_lengths().items())
    print('corners: %i, edges: %i, triangles: %i' % (len(msh.points), len(msh.edges), len(msh.triangles)))
    print('lencount: %i, minlen: %0.4f, maxlen: %0.4f' % (len(lengths), lengths[0][0], lengths[-1][0]))
    print('count of edge lengths:\n%s' % '\n'.join(map(lambda e: '%8.4f: %d' % (e[0], e[1]), lengths)))
    fname = 'tetraeder%d'%n
    open(fname + '.py', 'w').write(msh.output_prginput('spere'))
    open(fname + '.beam', 'w').write(msh.output_beams(0.05))
    open(fname + '.scad', 'w').write(msh.output_scad_polyhedron())
    open(fname + '.scad', 'w').write(msh.output_scad_polyhedron('halfsphere'))

    for i in range(0):
        msh = tetraeder_2
        msh.refine(i)
        open('tetraeder%d.beam' % i, 'w').write(msh.output_beams(0.05/i))
    
