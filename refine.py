#! /usr/bin/env python
#! /usr/bin/env python3

import optparse
import math as m

import mesh
import point

def parse_args():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--initialbody', help='body to start with [%s|stdin|<file>]' % mesh.initialbodys,
                      dest='i', default='ikosaeder', action='store')
    parser.add_option('-o', '--output', help='target to write result to [stdout|<file>]',
                      dest='o', default='stdout', action='store')
    parser.add_option('-f', '--format', help='output format [%s]' % mesh.outputformats,
                      dest='f', default='prginput', action='store')
    parser.add_option('-l', '--level', help='refinement level, cut existing edges n times',
                      dest='l', default=1, action='store', type='int')
    parser.add_option('-s', '--size', help='how many to operate on [sphere|halfsphere|triangle|radiant from +z]',
                      dest='s', default='sphere', action='store')
    parser.add_option('-a', '--arc', help='refinement by partitioning arcs instead of chords',
                      dest='a', default=False, action='store_true')
    parser.add_option('-r', '--radius', help='radius of the generated sphere',
                      dest='r', default=1.0, action='store', type='float')
    parser.add_option('-R', '--rotate', help='rotate n radiant',
                      dest='R', default=0.0, action='store', type='float')
    parser.add_option('-A', '--axis', help='axis for rotation x y z, std: 1, 0, 0',
                      dest='A', default=(1., 0., 0.), action='store', type='float', nargs=3)
    # parse options
    (opts, args) = parser.parse_args()

    # check dependencys, if applicable
    class unknown_size_parameter(BaseException):
        '''the provided size must be either in: sphere|halfsphere|triangle or a floating point number'''
    if   opts.s == 'halfsphere': opts.s=m.pi/2+0.02
    elif opts.s == 'sphere': opts.s = m.pi
    elif opts.s == 'triangle': pass
    else: opts.s = float(opts.s)
#    else: raise unknown_size_parameter

    return opts, args

if __name__ == '__main__':
    (opts, args) = parse_args()

    msh = None
    inputmsh = ''

    if opts.i not in (mesh.initialbodys + '|stdin').split('|'):
        g = l = {}
        inputmsh = open(opts.i, 'r').read()
        inputmsh = eval('msh='+inputmsh, g, l)
        inputmsh = g['msh']
    elif opts.i == 'stdin':
        import sys
        inputmsh = sys.stdin.read()
        inputmsh = eval('msh='+open(opts.i, 'r').read(), g, l)
        inputmsh = g['msh']
    else:
        inputmsh = [mesh.tetraeder, mesh.oktaeder, mesh.cube, mesh.dodekaeder, mesh.ikosaeder][mesh.initialbodys.split('|').index(opts.i)]

    msh = mesh.mesh(inputmsh, opts.a, opts.s)
    msh.set_radius(opts.r)
    if opts.R:
        msh.rotate_around_origin(point.cartesian_point(*opts.A), opts.R)

    msh.refine(opts.l)

    result = msh.output(opts.f, opts.s)

    if opts.o == 'stdout': print(result)
    else: open(opts.o, 'w').write(result)



