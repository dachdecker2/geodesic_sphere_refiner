

import math as m

class point(object):
    '''creates a point object using the supplied spheric coordinates
    '''
    def __init__(self, r, theta, phi):
        self.r = r
        self.theta = theta % (2*m.pi)
        self.phi = phi
        if self.theta > m.pi:
            self.theta = 2*m.pi - self.theta
            self.phi += m.pi
        self.phi %= 2*m.pi

    def __add__(self, other):
        return cartesian_point(*map(lambda a, b: a+b, self.cartesian(), other.cartesian()))
        x1, y1, z1 = self.cartesian()
        x2, y2, z2 = other.cartesian()
        return cartesian_point(x1+x2, y1+y2, z1+z2)

    def __sub__(self, other):
        return cartesian_point(*map(lambda a, b: a-b, self.cartesian(), other.cartesian()))
        x1, y1, z1 = self.cartesian()
        x2, y2, z2 = other.cartesian()
        return cartesian_point(x1-x2, y1-y2, z1-z2)

    def cross_product(self, other):
        x1, y1, z1 = self.cartesian()
        x2, y2, z2 = other.cartesian()
        return cartesian_point(y1*z2-z1*y2, z1*x2-x1*z2, x1*y2-y1*x2)

    def dot_product(self, other):
        return sum(map(lambda a, b: a*b, self.cartesian(), other.cartesian()))

    def rotate_around_origin(self, axis, angel):
        y = axis.cross_product(self)  # obtain y coordinate for calculation (perpendicular to self and axis)
        x = axis.cross_product(y)  # obtain x coordinate for calculation (perpendicular to y and axis)
        x = y.cross_product(axis)  # obtain x coordinate for calculation (perpendicular to y and axis)
        perpendicular_foot = axis.copy()
        perpendicular_foot.r = self.dot_product(axis)/axis.dot_product(axis)
        perpendicular_len = (self-perpendicular_foot).length()
        x0 = x.copy()
        y0 = y.copy()
        r = (self.length()**2 - perpendicular_foot.length()**2)**0.5
        x.r = r * m.cos(angel)
        y.r = r * m.sin(angel)
#        print('self: %s, axis: %s, r: %f,\n   pf: %s\n   x0: %s, y0: %s,\n    x: %s, y: %s,\np+x+y: %s\n' % (
#            self, axis, r, perpendicular_foot, x0, y0, x, y, perpendicular_foot+x+y))
        return perpendicular_foot+x+y

    def cartesian(self):
        '''returns the cartesian coordinates of its self'''
        x = self.r * m.sin(self.theta) * m.cos(self.phi)
        y = self.r * m.sin(self.theta) * m.sin(self.phi)
        z = self.r * m.cos(self.theta)
        return x, y, z

    def distance(self, other):
        '''returns the distance to the supplied other point object'''
        x1, y1, z1 = self.cartesian()
        x2, y2, z2 = other.cartesian()
        return ((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)**0.5
    
    def length(self):
        '''returns the distance of this point to the origin'''
        return sum(map(lambda x: x**2, self.cartesian()))**0.5
    
    def copy(self):
        '''returns a new point object with the same content'''
        return point(self.r, self.theta, self.phi)
    
    def __str__(self):
        return '(%+5.2f,%+5.2f,%+5.2f)' % (self.cartesian())
    
    def __repr__(self):
        return '%+5.2f,%+5.2f,%+5.2f' % (self.cartesian())


def cartesian_point(x, y, z):
    '''returns a regular point object but expects cartesian coordinates'''
    r = (x**2 + y**2 + z**2)**0.5
    if r == 0: return point(0,0,0)
    theta = m.acos(z/r)
    phi = m.atan2(y, x)
    if phi < 0:
        phi = phi+m.pi
        theta = 2*m.pi-theta
    return point(r, theta, phi)


def test_coord_transform():
    print('%-45s%-45s%s' % ('theta:', 'phi:', '  deg ->     x,    y -> atan2[deg]'))
    for alpha in range(0, int(m.pi*16)+1, 1):
#        p = point(1,alpha/8.,m.pi/2)
        p = point(1,alpha/8.,0.5*m.pi/2)
        p2 = cartesian_point(*p.cartesian())
        r1 = str(p) + '  %+5.2f,%+5.0f,%+5.0f' % (p2.r, p2.theta*180/m.pi, p2.phi*180/m.pi)
        
        p = point(1, m.pi/2, alpha/8.)
        p2 = cartesian_point(*p.cartesian())
        r2 = str(p) + '  %+5.2f,%+5.0f,%+5.0f' % (p2.r, p2.theta*180/m.pi, p2.phi*180/m.pi)
        if alpha/8. > m.pi: r2 = ''
        
        x = m.cos(alpha/8.)
        y = m.sin(alpha/8.)
        r3 = '%+5.0f -> %+5.2f,%+5.2f -> %+5.0f' % (alpha/8.*180/m.pi, x, y, m.atan2(y,x)*180/m.pi)
        print('%-45s%-45s%s' % (r1, r2, r3))

def rotate_around_origin_test():
    p1 = cartesian_point(1, 0, 1)
    axis = cartesian_point(0, 0, 1)
    rot = p1.rotate_around_origin(axis, m.pi/4)
    pass

if __name__ == '__main__':
#    test_coord_transform()
    rotate_around_origin_test()

